import { Component, OnInit } from '@angular/core';
import { swingIconAnimation } from '../../animations/swing-icon.animation';

@Component({
  selector: 'nc-badge',
  templateUrl: './badge.component.html',
  styleUrls: ['./badge.component.scss'],
  animations: [ swingIconAnimation ]
})
export class BadgeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
